<?php
/**
 * @file
 * Contains \Drupal\subscribenewsletter\Plugin\Block\SubscribeNewsletterBlock.
 */

namespace Drupal\subscribenewsletter\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'SubscribeNewsletter' block.
 *
 * @Block(
 *   id = "subscribenewsletter_block",
 *   admin_label = @Translation("Subscribe Newsletter Block"),
 *   category = @Translation("Subscribe Newsletter Block")
 * )
 */
class SubscribeNewsletterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $form = \Drupal::formBuilder()->getForm('Drupal\subscribenewsletter\Form\SubscribeNewsletterForm');
    $config = \Drupal::config('subscribenewsletter.subscribeendpoints');
    $fid = $config->get('fid_logo');
    $file = File::load($fid);
    $file_uri = $file->getFileUri();
    $file_path = \Drupal::service('file_url_generator')->generateAbsoluteString($file_uri);
    
     $renderable = [
      '#theme' => 'subscribenewsletter_block',
      '#icon' => $file_path,
      '#title' => $config->get('title'),
      '#description' => $config->get('description'),
      '#form' => $form,
    ];

    return $renderable;
   }

}