<?php
/**
 * @file
 * Contains \Drupal\subscribenewsletter\Form\SubscribeNewsletterForm.
 */
namespace Drupal\subscribenewsletter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class SubscribeNewsletterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscribe_newsletter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['email'] = [
      '#type' => 'textfield',
      '#placeholder' => 'Email ID',
      '#size' => 60,
      '#maxlength' => 100,
      '#required' => TRUE,
      '#prefix' => '<div class="form-group input-container mb-3 mb-lg-0 mr-0 mr-lg-2"><div class="form-wrapper">',
      '#suffix' =>'</div></div>'
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Subscribe'),
    ];
    $form['#attributes']['class'][] = 'd-flex flex-column flex-lg-row align-items-center';
    $form['email']['#attributes']['class'][] = 'form-control';
    //$form['email']['#attributes']['id'][] = 'subscribe-mail-id';
    $form['actions']['submit']['#attributes']['class'][] = 'arrow-red btn btn-secondary center-v';
    //$form['actions']['submit']['#attributes']['class'][] = ''
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
    parent::validateForm($form, $form_state);

    $email = $form_state->getValue('email');

    if ($email !== '' && !\Drupal::service('email.validator')->isValid($email)) {
      $form_state->setErrorByName('email', $this->t('Invalid email address'));  
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Display the results.
    /**
     * Get the email address to which email to be sent 
     */
    
    try {
      $config = \Drupal::config('subscribenewsletter.subscribeendpoints');
      $url = $config->get('endpoint_url').'&'.$config->get('API_Key');
      $client = \Drupal::httpClient();
      $request = $client->post($url, 
                   ['json' => ['EMAIL'=> $form_state->getValue('email')],
                  'Content-Type' => 'application/x-www-form-urlencoded'
                  ]);
      $response = json_decode($request->getBody());
      \Drupal::logger('NewsletterSignupForm')->notice(serialize($response));
    }
    catch (RequestException $e) {
      watchdog_exception('subscribenewsletter', $e->getMessage());
    }

    // Call the Static Service Container wrapper
    $messenger = \Drupal::messenger();
    $messenger->addMessage('You have subscribed for updates successfully with Email: '.$form_state->getValue('email'));

    // Redirect to home
    $form_state->setRedirect('<front>');

  } 

}