<?php

namespace Drupal\subscribenewsletter\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface; 

/**
 * Provides a configuration form for Newletter Subscription.
 */
class EndpointForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscribenewsletter_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['subscribenewsletter.subscribeendpoints'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $site_config = $this->config('subscribenewsletter.subscribeendpoints');

    $form['newsletter_api_form'] = [
      '#type' => 'details',
      '#title' => t('API endpoints information'),
      '#open' => TRUE,
    ];
    $form['newsletter_api_form']['title'] = [
      '#type' => 'textfield',
      '#title' => t('Subscribe Newsletter Block Title'),
      '#default_value' => $site_config->get('title'),
      '#required' => TRUE,
    ];
    $form['newsletter_api_form']['description'] = [
      '#type' => 'textfield',
      '#title' => t('Subscribe Newsletter Block Description'),
      '#default_value' => $site_config->get('description'),
      '#required' => TRUE,
    ];
    $form['newsletter_api_form']['api_endpoint_url'] = [
      '#type' => 'textfield',
      '#title' => t('Newsletter Subscribe endpoint URL'),
      '#default_value' => $site_config->get('endpoint_url'),
      '#required' => TRUE,
    ];
    $form['newsletter_api_form']['x_api_key_value'] = [
      '#type' => 'textfield',
      '#title' => t('X API Key'),
      '#default_value' => $site_config->get('API_Key'),
      '#required' => TRUE,
    ];
    $form['newsletter_api_form']['fid_logo_value'] = [
      '#type' => 'managed_file',
      '#title' => t('Default Logo Image'),
      '#description' => t('Set the logo for newsletter block'),
      '#upload_location' => 'public://files',
      '#default_value' => [$site_config->get('fid_logo')],
      '#upload_validators'    => [
        'file_validate_extensions'    => ['webp png jpg jpeg'],
        'file_validate_size'          => [25600000]
      ],
    ];
  

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  
    if ($form_state->isValueEmpty('api_endpoint_url')) {
      $form_state->setErrorByName('api_endpoint_url', $this->t("The path cannot be empty.", ['%path' => $form_state->getValue('api_endpoint_url')]));
    }
    if ($form_state->isValueEmpty('x_api_key_value')) {
      $form_state->setErrorByName('x_api_key_value', $this->t("key cannot be empty.", ['%key' => $form_state->getValue('x_api_key_value')]));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file_data = $form_state->getValue('fid_logo_value');
    $file = File::load( $file_data[0] );
    $file_name = $file->getFilename();
    $file->setPermanent();
    $file->save();
    $this->config('subscribenewsletter.subscribeendpoints')
      ->set('title', $form_state->getValue('title'))
      ->set('description', $form_state->getValue('description'))
      ->set('endpoint_url', $form_state->getValue('api_endpoint_url'))
      ->set('API_Key', $form_state->getValue('x_api_key_value'))
      ->set('fid_logo',$form_state->getValue('fid_logo_value')[0])
      ->save();
      
    parent::submitForm($form, $form_state);
  }

}